package com.example.tuparquekotlin.ui


import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment

import com.example.tuparquekotlin.R
 /**
 * A simple [Fragment] subclass.
 *
 */
class AhoraFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ahora, container, false)
    }


}
