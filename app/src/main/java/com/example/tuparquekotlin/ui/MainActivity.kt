package com.example.tuparquekotlin.ui

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.tuparquekotlin.R
import com.example.tuparquekotlin.ui.modelo.Parques
import com.example.tuparquekotlin.viewModel.viewmodel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    //mostrar posibles cambios del LiveData
    private lateinit var viewmodelo:viewmodel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Japon.setOnClickListener {

            val intent:Intent = Intent(this, logInActivity::class.java)
            startActivity(intent)
        }

        Nogal.setOnClickListener {

            val intent:Intent = Intent(this, logInActivity::class.java)
            startActivity(intent)
        }

        //instancia del viewmodel
        viewmodelo = ViewModelProviders.of(this).get(viewmodel::class.java)

        //cambios obtenidos por el liveData
        val observadorParques = Observer<List<Parques>>
        {
            for(x in it)
            {
                Log.d("Parques: ",x.nombreParque)
            }
        }

        viewmodelo.getListaParqueaderosLiveData().observe(this,observadorParques)

        val cm = baseContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val networkInfo = cm.activeNetworkInfo

        if (networkInfo != null && networkInfo.isConnected){
            Toast.makeText(baseContext, "Conctado a internet", Toast.LENGTH_SHORT).show()
        }
        else {
            Toast.makeText(baseContext, "No tiene conexión a internet", Toast.LENGTH_SHORT).show()
        }


    }
}
