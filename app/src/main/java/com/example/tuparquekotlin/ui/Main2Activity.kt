package com.example.tuparquekotlin.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tuparquekotlin.R
import kotlinx.android.synthetic.main.activity_main2.*

class Main2Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        //título toolbar
        //toolBar.setTitle("Tab layout")
        //setSupportActionBar(toolBar)

        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewPager.adapter = fragmentAdapter

        tabLayout.setupWithViewPager(viewPager)

    }
}
