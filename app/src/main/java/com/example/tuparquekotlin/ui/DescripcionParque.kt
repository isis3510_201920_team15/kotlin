package com.example.tuparquekotlin.ui

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.tuparquekotlin.R
import kotlinx.android.synthetic.main.activity_descripcion_parque.*
import kotlinx.android.synthetic.main.activity_descripcion_parque.tabLayout
import kotlinx.android.synthetic.main.activity_main2.*


class DescripcionParque : AppCompatActivity() {

    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_descripcion_parque)

        mSectionsPagerAdapter = SectionsPagerAdapter(supportFragmentManager)


        Mapa.setOnClickListener {
            val intent: Intent = Intent(this, MapsActivity::class.java)
            startActivity(intent)
        }


        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewPager.adapter = fragmentAdapter

        tabLayout.setupWithViewPager(viewPager)

        val cm = baseContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val networkInfo = cm.activeNetworkInfo

        if (networkInfo != null && networkInfo.isConnected){
            Toast.makeText(baseContext, "Conctado a internet", Toast.LENGTH_SHORT).show()
        }
        else {
            Toast.makeText(baseContext, "No tiene conexión a internet", Toast.LENGTH_SHORT).show()
        }

    }

    inner class SectionsPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {

            when (position) {
                0 -> {
                    return DetallesFragment()

                }
                1 -> {
                    return ReviewsFragment()
                }
                2 -> {
                    return AhoraFragment()
                }
                else -> {
                    return DetallesFragment()
                }
            }
        }

        override fun getCount(): Int {
            return 3
        }
    }
}
