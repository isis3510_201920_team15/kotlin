package com.example.tuparquekotlin.ui

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.tuparquekotlin.R
import kotlinx.android.synthetic.main.activity_log_in.*

class logInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)


        Cancelar.setOnClickListener {

            val intent:Intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }

        aceptar.setOnClickListener {

            val intent:Intent = Intent(this, DescripcionParque::class.java)
            startActivity(intent)
        }

        val cm = baseContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val networkInfo = cm.activeNetworkInfo

        if (networkInfo != null && networkInfo.isConnected){
            Toast.makeText(baseContext, "Conctado a internet", Toast.LENGTH_SHORT).show()
        }
        else {
            Toast.makeText(baseContext, "No tiene conexión a internet", Toast.LENGTH_SHORT).show()
        }

    }
}
