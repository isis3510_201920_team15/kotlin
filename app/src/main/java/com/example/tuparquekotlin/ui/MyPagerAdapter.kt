package com.example.tuparquekotlin.ui

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class MyPagerAdapter (fm:FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                DetallesFragment()
            }
            1 -> {
                ReviewsFragment()
            }
            else -> {
                return AhoraFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position){
            0-> "Detalles"
            1-> "Reseñas"
            else -> return "Ahora"
        }
    }
}