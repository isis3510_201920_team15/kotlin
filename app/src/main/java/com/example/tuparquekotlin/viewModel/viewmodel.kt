package com.example.tuparquekotlin.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.tuparquekotlin.domain.ParquesUseCase
import com.example.tuparquekotlin.ui.modelo.Parques

class viewmodel:ViewModel() {

    private val listData = MutableLiveData<List<Parques>>()
    //Mutable porque cambia

    val parquesUseCase = ParquesUseCase()
    //llamo al useCase

    init
    {
        getListadoParques()
        //obtengo la lista de parques desde que inicio
    }

    fun setListData(listadoParques:List<Parques>)
    {
        listData.value = listadoParques
    }

    fun getListadoParques()
    {
        setListData(parquesUseCase.obtenerListaParques())
        //le setea a la MutableLiveData la lista de parques
    }

    fun getListaParqueaderosLiveData():LiveData<List<Parques>>
    //Para saber su toca cambiar el Mutable, el LiveData revisa si ha habido cambios, se ejecuta en el momento que llegue la info de parques
    {
        return listData
    }
}
