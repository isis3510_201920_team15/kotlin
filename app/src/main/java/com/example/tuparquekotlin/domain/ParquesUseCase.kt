package com.example.tuparquekotlin.domain

import com.example.tuparquekotlin.data.RepoParques
import com.example.tuparquekotlin.ui.modelo.Parques

class ParquesUseCase {

    private val repoParques = RepoParques()
    //llamo a la BD donde tengo la información de los parques

    fun obtenerListaParques():List<Parques>
    {
        return repoParques.crearListaParques()
    }

}
